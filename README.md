
Des modèles pour mettre en forme le contenu de vos newsletters avec le plugin Newsletter.


# Mode d'emploi

## Importer les styles nécessaires aux modèles

Dans la balise `<style>` de votre template de newsletter, juste avant les Media queries (@media), insérer `[(#PLUGIN{newsletters_modeles}) <INCLURE{fond=css/newsletters_modeles.css} /> ]`

## Afficher les modèles dans le contenu

### Sélections éditoriales `<newsletters_selectionX>`

Créez une selection éditoriale, ajoutez-y du contenu puis insérez-là dans le contenu de votre newsletter avec l'un des modèles suivants :
- Affichage pleine largeur `<newsletters_selectionX>`
- Affichage en ligne `<newsletters_selectionX|affichage=ligne>`
- Affichage en colonne `<newsletters_selectionX|affichage=colonne>`

Voir le source pour les paramètres du modèle : /modeles/newsletters_selection.html

**Résultat :** https://stk.cousumain.info/capture_newsletters_modeles.jpg

**Note :** Le modèle `<newsletters_selectionX>` utilise le bouton bulletproof de Newsletters.

### Bouton `<newsletters_bouton>`

Ce modèle utilise le bullet proof button de Newsletter (voir https://git.spip.net/spip-contrib-extensions/newsletters/src/branch/master/newsletters/inc/button.html)

Syntaxte `<newsletters_bouton>`

**Personnalisation du bouton :**
Choisir le texte : `|texte_bouton=Cliquer ici`
Choisir le lien : `|lien=https://www.domaine.fr`

Exemple : `<newsletters_bouton|texte_bouton=Cliquer ici|lien=https://www.domaine.fr>`

# Plus d'infos 
- Post initial sur Seenthis : https://seenthis.net/messages/800984
- discussion commencée sur la liste spip-zone https://www.mail-archive.com/spip-zone@rezo.net/msg48309.html
- puis continuée sur spip-dev https://www.mail-archive.com/spip-dev@rezo.net/msg69104.html
- annonce sur spip-dev : https://www.mail-archive.com/spip-dev@rezo.net/msg69245.html